return {
  "mfussenegger/nvim-dap",
    -- stylua: ignore
   keys = {
    { "<F32>", function() require("dap").toggle_breakpoint() end, desc = "Toggle Breakpoint" }, -- Ctrl+F8
    { "<F21>", function() require("dap").continue() end, desc = "Continue" }, -- Shift+F9
    -- { "<M-S-F9>", function() require("dap").run_to_cursor() end, desc = "Run to Cursor" }, -- Alt+Shift+F9
    { "<F7>", function() require("dap").step_into() end, desc = "Step Into" }, -- F7
    { "<F20>", function() require("dap").step_out() end, desc = "Step Out" }, -- Shift+F8
    { "<F8>", function() require("dap").step_over() end, desc = "Step Over" }, -- F8
    { "<F14>", function() require("dap").terminate() end, desc = "Terminate" }, -- Shift+F2
  },
}
