return {
  "stevearc/conform.nvim",
  opts = function(_, opts)
    -- sql
    local sql_ft = { "sql", "mysql", "plsql" }
    opts.formatters.sqlfluff = {
      args = { "format", "-" },
    }
    for _, ft in ipairs(sql_ft) do
      opts.formatters_by_ft[ft] = opts.formatters_by_ft[ft] or {}
      table.insert(opts.formatters_by_ft[ft], "sqlfluff")
    end

    -- cmake
    opts.formatters_by_ft["cmake"] = { "cmake_format" }

    -- protobuf
    opts.formatters_by_ft["proto"] = { "clang-format" }
  end,
}
