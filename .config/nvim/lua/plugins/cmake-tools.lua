local ft = { "cmake", "c", "cpp" }

return {
  "Civitasv/cmake-tools.nvim",
  lazy = true,
  ft = ft,
  opts = {
    cmake_regenerate_on_save = false,
    cmake_soft_link_compile_commands = false,
    cmake_compile_commands_from_lsp = true,
  },
  keys = {
    { "<F33>", "<cmd>CMakeBuild<cr>", ft = ft, desc = "Build CMake Project" }, -- Ctrl+F9
    { "<F22>", "<cmd>CMakeRun<cr>", ft = ft, desc = "Run CMake Project" }, -- Shift+F10
    { "<M-S-F9>", "<cmd>CMakeSelectConfigurePreset<cr>", ft = ft, desc = "Select CMake Configure Type" }, -- Alt+Shift+F9
    { "<F45>", "<cmd>CMakeSelectBuildPreset<cr>", ft = ft, desc = "Select CMake Build Preset" }, -- Ctrl+Shift+F9
    { "<M-S-F10>", "<cmd>CMakeSelectLaunchTarget<cr>", ft = ft, desc = "Select CMake Launch Target" }, -- Alth+Shift+F10
  },
}
