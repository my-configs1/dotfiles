return {
  "neovim/nvim-lspconfig",
  init = function()
    local keys = require("lazyvim.plugins.lsp.keymaps").get()
    keys[#keys + 1] = {
      "gl",
      function()
        vim.diagnostic.open_float()
      end,
    }
  end,
  ---@class PluginLspOpts
  opts = {
    setup = {
      rust_analyzer = function()
        return true
      end,
    },
    servers = {
      bashls = {},
      clangd = {
        filetypes = { "c", "cpp", "objc", "objcpp", "cuda", "hpp" },
        cmd = {
          "clangd",
          "--clang-tidy",
          "--enable-config",
          "--background-index",
          "--header-insertion=iwyu",
          "--fallback-style=Microsoft",
          "--completion-style=detailed",
          "--function-arg-placeholders",
          "--query-driver=/**/xtensa-*-elf-*",
          "--query-driver=/**/mips-*-linux-*",
          "--query-driver=/**/x86_64-*-linux-*",
        },
        on_new_config = function(new_config, _)
          local cmake_tools_ok, cmake_tools = pcall(require, "cmake-tools")
          if cmake_tools_ok then
            cmake_tools.clangd_on_new_config(new_config)
          end
        end,
      },
      neocmake = {
        default_config = {
          init_options = {
            format = {
              enable = false,
            },
          },
        },
      },
      protols = {
        filetypes = { "proto" },
      },
    },
  },
}
