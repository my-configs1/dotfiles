local utils = require("config.utils").lang.c

return {
  "mfussenegger/nvim-lint",
  opts = {
    linters_by_ft = {
      cpp = { "cppcheck", "clazy" },
      cmake = { "cmake-lint" },
    },
    linters = {
      clazy = {
        args = {
          "--checks=level0,level1",
          function()
            local cmake_build_dir = utils.get_cmake_build_dir()
            if cmake_build_dir then
              return "-p", cmake_build_dir
            else
              return ""
            end
          end,
        },
      },
      cppcheck = {
        args = {
          function()
            local cmake_build_dir = utils.get_cmake_build_dir()
            if cmake_build_dir then
              return "--cppcheck-build-dir=" .. cmake_build_dir, "--enable=all"
            else
              return "--enable=warning,style,performance,information"
            end
          end,
          function()
            if vim.bo.filetype == "cpp" then
              return "--language=c++"
            else
              return "--language=c"
            end
          end,
          "--check-level=exhaustive",
          "--force",
          "--quiet",
          "--inline-suppr",
          "--template={file}:{line}:{column}: [{id}] {severity}: {message}",
        },
      },
      ["cmake-lint"] = {
        cmd = "cmake-lint",
        stdin = false,
        args = { "--suppress-decorations" },
        ignore_exitcode = true,
        parser = require("lint.parser").from_pattern(
          "([^:]+):(%d+): %[(.+)%] (.+)",
          { "file", "lnum", "code", "message" },
          nil,
          {
            ["source"] = "cmakelint",
            ["severity"] = vim.diagnostic.severity.WARN,
          }
        ),
      },
      sqlfluff = {
        args = {
          "lint",
          "--format=json",
        },
      },
    },
  },
}
