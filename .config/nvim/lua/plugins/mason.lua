return {
  "williamboman/mason.nvim",
  opts = {
    ensure_installed = {
      "shellcheck",
      "shfmt",
      "slint-lsp",
      "protols",
    },
  },
}
