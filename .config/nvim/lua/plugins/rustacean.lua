return {
  "mrcjkb/rustaceanvim",
  lazy = true,
  keys = {
    {
      "<F22>",
      function()
        vim.cmd.RustLsp("runnables")
      end,
      ft = { "rust" },
      desc = "Rust Runnables",
    }, -- Shift+F10
  },
  opts = function(_, opts)
    opts.server.default_settings = {
      ["rust-analyzer"] = {
        procMacro = {
          enable = true,
          ignored = {},
        },
      },
    }
  end,
}
