-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here

vim.api.nvim_create_autocmd("FileType", {
  desc = "C/C++ tabbing",
  pattern = { "c", "cpp" },
  callback = function()
    vim.opt.shiftwidth = 0
    vim.opt.tabstop = 4
  end,
})
