-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

vim.opt.clipboard = "" -- Don't sync with system clipboard

-- vimtex
vim.g.vimtex_fold_manual = 1
vim.g.vimtex_view_method = "zathura"
vim.g.vimtex_imaps_enabled = 0
vim.g.vimtex_complete_enabled = 0
