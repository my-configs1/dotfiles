local M = {
  lang = {
    c = {},
    tex = {},
  },
}

---@return string?
M.lang.c.get_cmake_build_dir = function()
  local cmake_tools_ok, cmake_tools = pcall(require, "cmake-tools")
  if cmake_tools_ok then
    return cmake_tools.get_build_directory().filename
  end

  local files = vim.fs.find(function(name, _) -- args: name & path
    return name:match("cmake%-build%-*")
  end, { limit = 1, type = "directory" })
  if files then
    return files[1]
  else
    return nil
  end
end

---@return boolean
M.lang.tex.in_math_zone = function()
  return vim.fn["vimtex#syntax#in_mathzone"]() == 1
end

return M
