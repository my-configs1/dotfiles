#
# ~/.bashrc
#

export LESS="-SXR"
export MANPAGER='less -sX -M +Gg'
export EDITOR="nvim"
export HISTCONTROL='ignoreboth'
export SDK_ROOT=/opt/sdks
export TOOLCHAIN_ROOT=/usr
export ANDROID_NDK=${SDK_ROOT%/}/android/ndk/28.0.13004108
export ANDROID_HOME=/opt/sdks/android/
export NDK_HOME=$ANDROID_NDK
export WEBKIT_DISABLE_DMABUF_RENDERER=1
export CHROME_EXECUTABLE=brave-browser-stable

# go
export PATH=$HOME/go/bin:$PATH

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH=$BUN_INSTALL/bin:$PATH

# Created by `pipx` on 2023-07-29 12:52:45
export PATH="$PATH:/home/omar/.local/bin"

# krew
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

# flutter
export PATH="/opt/flutter/bin:$PATH"

# android
export PATH="$ANDROID_HOME/platform-tools:$PATH"

# cargo
. "$HOME/.cargo/env"

alias vi='nvim'
alias hostname='hostnamectl hostname'
alias k='kubectl'

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#export GPG_TTY="$(tty)"

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then
  source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"
fi
# END_KITTY_SHELL_INTEGRATION

complete -C /usr/local/bin/vault vault
