#
# ~/.bash_profile
#

# Created by `pipx` on 2023-07-29 12:52:45
export PATH="$PATH:/home/omar/.local/bin"

# always run bashrc
[[ -f ~/.bashrc ]] && . ~/.bashrc

# use colors for less
[[ -f ~/.LESS_TERMCAP ]] && . ~/.LESS_TERMCAP

