#!/bin/bash
full_script_path=$(realpath "$(pwd)/$(dirname "$0")")

function copy_to_from() {
    if [ $# != 3 ]; then
        echo "$0 [f|d] <target> <source>" >&2
        return 255
    fi

    type=$1
    target=$2
    source=$3

    if [[ $type != 'f' ]] && [[ $type != 'd' ]]; then
        echo "must specify whether to copy files or directories" >&2
        echo "$0 [f|d] <target> <source>" >&2
        return 255
    fi

    if [[ $type = 'd' ]]; then
        files=$(find "$3" -maxdepth 1 -type "$1" ! -name "*.sh" | tail --lines=+2)
    elif [[ $type = 'f' ]]; then
        files=$(find "$3" -maxdepth 1 -type "$1" ! -name "*.sh")
    fi

    for file in $files; do
        file=$(basename "$file")
        echo "$file"
        # back up the already existing files
        eval "[ -$type ${target:?}/${file} ] && [ ! -h ${target:?}/${file} ]" && cp -rf "${target:?}/${file}" "${target:?}/${file}.bak"

        rm -rf "${target:?}/${file}"
        ln -s "$(realpath "${full_script_path:?}/${source:?}/${file}")" "${target:?}/${file}"
    done
}

pushd "${full_script_path}" || exit 255
copy_to_from 'f' ~ .
copy_to_from 'd' ~/.config .config
popd || exit 255

git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
